# Renishaw File Reader #
### Alex Henderson ###

This source code will read a Renishaw file.

The Renishaw file format for their WiRE software comes in different versions. This code will read version 4+, which has a file extension of .wdf

The source code is released under the MIT License. If you would like to use the code under different licensing conditions, please contact the author.

If you find this code fails for whatever reason, please let the author know using the issues section of the BitBucket website: [https://bitbucket.org/AlexHenderson/renishaw-file-formats/issues](https://bitbucket.org/AlexHenderson/renishaw-file-formats/issues)

If you make use of this file in your work, please consider citing the appropriate version. Release 1.0 (GPL-3 license) is available in the Zenodo Repository: [DOI:10.5281/zenodo.495477](http://doi.org/10.5281/zenodo.495477 "DOI:10.5281/zenodo.495477"). Release 2.0, which is exactly the same code, but with a more permissive MIT License, is available from BitBucket [https://bitbucket.org/AlexHenderson/renishaw-file-formats](https://bitbucket.org/AlexHenderson/renishaw-file-formats)

Alex Henderson <[alex.henderson@manchester.ac.uk](mailto:alex.henderson@manchester.ac.uk "alex.henderson@manchester.ac.uk")>

## Usage ##

### MATLAB Version ###
	%   Function: renishawWire
	%   Usage: [ramanshift,data,height,width,filename,acqdate,x_label,y_label] = renishawWire();
	%   Usage: [ramanshift,data,height,width,filename,acqdate,x_label,y_label] = renishawWire(filename);
	%   Usage: [ramanshift,data,height,width,filename,acqdate,x_label,y_label] = renishawWire(filename,plotContents);
	%
	%   Extracts the data from a Renishaw .wdf file.
	%   Currently only reads version 4+ of WiRE. 
	%   Optionally plots the contents of the file as either a single spectrum,
	%   a series of spectra overlaid, or an image or spectrum of the total
	%   signal.
	%
	%   input:
	%   'filename' string containing the full path to the .wdf file (optional)
	%   'plotContents' 1 or 0 where 1 means plot the contents (default) and 0
	%       means do not (optional). Only used when a filename is provided. 
	% 
	%   output:
	%   'ramanshift' is a list of the Raman shifts related to the data
	%   'data' is a 3D hypercube of the data in the file (height x width x ramanshifts)
	%   'filename' is a string containing the full path to the .wdf file
	%   'acqdate' is a string containing the date and time of acquisition
	%   'x_label' is the name of the unit on the x axis (Raman shift in wavenumber)
	%   'y_label' is the name of the unit on the y axis (intensity)
	%
	%                     *******Caution******* 
	%   This code is a hack of the WiRE format and the location of the data
	%   within the file may vary. Always check the output to make sure it is
	%   sensible. If you have a file that doesn't work, please contact Alex. 
	%
	%   Copyright (c) 2014 - 2022, Alex Henderson 
	%   Contact email: alex.henderson@manchester.ac.uk
	%   Now licensed under the MIT License
	%   https://opensource.org/licenses/MIT
	%   Other licensing options are available, please contact Alex for details
	%   If you use this file in your work, please acknowledge the author(s) in
	%   your publications. 
